#include "image_filter.h"

extern void sepia_asm(float const *matrix, struct Pixel *pixels, size_t size);

static unsigned char sat(uint64_t x) {
    if (x < 256) return x;
    return 255;
}

void naive_sepia_pixel(struct Pixel* pixel) {
    static const float c[3][3] = {
            {.272f, .543f, .131f},
            {.349f, .686f, .168f},
            {.393f, .769f, .189f}};
    struct Pixel const old = *pixel;
    pixel->r = sat(
            old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2]
    );
    pixel->g = sat(
            old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2]
    );
    pixel->b = sat(
            old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2]
    );
}

void naive_sepia(struct Image* image) {
    struct Pixel* pixel;
    for (int i = 0; i < image->height * image->width; i++) {
        pixel = &image->imageData[i];
        naive_sepia_pixel(pixel);
    }
}

void sse_sepia(struct Image* image) {
    float cc[9] = {.272f, .543f, .131f, .349f, .686f, .168f, .393f, .769f, .189f};
    size_t i;
    size_t full_size = image->width * image->height;
    size_t sse_num = full_size / 4;
    if (sse_num)
        sepia_asm(cc, image->imageData, sse_num);
    for (i = 4 * sse_num; i < full_size; ++i)
        naive_sepia_pixel(&image->imageData[i]);
}