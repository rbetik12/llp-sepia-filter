#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>
#include <sys/resource.h>
#include "image_parser.h"
#include "image_filter.h"

void compare_to_naive(const char* input_filename, const char* output_filename, long sse_res) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    struct Image* image = load(input_filename);

    if (image == NULL) {
        fprintf(stderr, "Can't load image!\n");
        exit(EXIT_FAILURE);
    }

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    naive_sepia(image);

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res_naive = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for naive filter: %.2f\n", res_naive / 100000.0f);

    printf("\033[1;31m");
    printf("SSE version is %.1f times faster!\n", (float) res_naive / (float) sse_res);
    printf("\033[0m");

    char* buffer = malloc(strlen(output_filename) + 9);
    snprintf(buffer, strlen(output_filename) + 9, "naive_%s", output_filename);
    save(buffer, image);
    free(image);
}

int main(int argc, char** argv) {
    if (argc < 3) {
        fputs("sepia <input image path> <output image path> [--compare]\n", stderr);
        exit(EXIT_FAILURE);
    }

    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    bool compare = false;
    if (argc >= 4 && strcmp(argv[3], "--compare") == 0) {
        compare = true;
    }

    struct rusage r;
    struct timeval start;
    struct timeval end;

    struct Image* image = load(input_filename);

    if (image == NULL) {
        fprintf(stderr, "Can't load image!\n");
        exit(EXIT_FAILURE);
    }

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    sse_sepia(image);

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for SSE filter: %.2f\n", res / 100000.0);

    save(output_filename, image);
    free(image);

    if (compare) {
        compare_to_naive(input_filename, output_filename, res);
    }
    return 0;
}
